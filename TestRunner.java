package stepDefention;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

 
@RunWith(Cucumber.class)
@CucumberOptions(features ="src/test/resources/feature/cucumber.feature", glue ="stepDefention",plugin = { "html:target/cucumber-reports"},
monochrome = true)
	public class TestRunner {

	} 


