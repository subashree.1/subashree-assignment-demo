package stepDefention;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Stepdefinition {
	static WebDriver driver=null;
	@Given("Open the website")
	public void open_the_website() {
	     driver=new ChromeDriver();
	     driver.get( " https://demowebshop.tricentis.com/");
		  driver.manage().window().maximize();
	}

	@Then("The homepage will be display")
	public void the_homepage_will_be_display() {
	     System.out.println("homepage");
	}

	@When("click on login button")
	public void click_on_login_button() {
		driver.findElement( By.xpath( "//a[text()='Log in']")).click();
	}

	@When("Enter the value Email id in the email textbox")
	public void enter_the_value_email_id_in_the_email_textbox() {
		driver.findElement( By.name( "Email")).sendKeys( "manzmehadi1@gmail.com");
	}

	@When("Enter the password in the password textbox")
	public void enter_the_password_in_the_password_textbox() {
		 driver.findElement( By.name( "Password")).sendKeys( "Mehek@110");
	}

	@Then("click on the login button")
	public void click_on_the_login_button() throws InterruptedException {
		//driver.findElement( By.xpath( "//input[@class='button-1 login-button']")).click();
		 WebElement loginButton = driver.findElement(By.cssSelector("input[value='Log in']"));
	      loginButton.click();
	       
	}

	@Then("the home page with login user name should display")
	public void the_home_page_with_login_user_name_should_display() {
	     System.out.println("Stepdefinition.the_home_page_with_login_user_name_should_display()");
	}

	@Then("I open the book")
	public void i_open_the_book() {
		 driver.findElement( By.xpath( "//a[@href=\"/books\"][1]")).click();
	}

	@Then("I add the first book to function")
	public void i_add_the_first_book_to_function() {
		 WebElement sel= driver.findElement( By.id( "products-orderby"));
		 Select s=new Select( sel);
		 s.selectByVisibleText( "Price: High to Low");
		 driver.findElement( By.xpath("//img[@title='Show details for Fiction']")).click();
	}

	@Then("I add the first book to shopping cart from function")
	public void i_add_the_first_book_to_shopping_cart_from_function() {
		 driver.findElement( By.id( "add-to-cart-button-45")).click();
	}

	@Then("I add the second book to science")
	public void i_add_the_second_book_to_science() {
		driver.findElement( By.xpath( "//a[@href=\"/health\"][1]")).click();
	}

	@Then("I add the second book to shopping cart from science")
	public void i_add_the_second_book_to_shopping_cart_from_science() {
		driver.findElement( By.id( "add-to-cart-button-22")).click();
	}

	@Then("click on  electronic")
	public void click_on_electronic() {
		WebElement  c = driver.findElement( By.xpath( "//a[@href=\"/electronics\"][1]"));
		 Actions act= new Actions(driver);
		 act.moveToElement(c).build().perform();
	}

	@Then("click on cell phone")
	public void click_on_cell_phone() {
		 driver.findElement( By.xpath( "//a[@href=\"/cell-phones\"]")).click();
	}

	@Then("I add the phone to smartphone")
	public void i_add_the_phone_to_smartphone() {
		driver.findElement( By.xpath( "//a[@href=\"/smartphone\"]")).click();
	}

	@Then("I add the shopping cart")
	public void i_add_the_shopping_cart() {
		 driver.findElement( By.id( "add-to-cart-button-43")).click();
		 String text = driver.findElement(By.xpath("//span[@class='cart-qty']")).getText();
	     System.out.println("the no of items in the cart is ---"+text);
	     
	}

	@Then("click on giftcard button")
	public void click_on_giftcard_button() {
		driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Gift Cards')]")).click();

	}

	@Then("verify {int} is displayed")
	public void verify_is_displayed(Integer int1) {
		 WebElement page = driver.findElement(By.id("products-pagesize"));
	     Select s1 = new Select(page);
	     s1.selectByVisibleText("4");
	}

	@Then("select a gift card and displayed its details")
	public void select_a_gift_card_and_displayed_its_details() {
		String coupan = driver.findElement(By.xpath("//a[contains(text(),'$100 Physical Gift Card')]")).getText();
	     System.out.println(coupan);
	}

	@Then("click on logout button")
	public void click_on_logout_button() {
		//driver.findElement(By.xpath("//a[@class='ico-logout']")).click();
	}
}
